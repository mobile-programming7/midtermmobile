import 'package:flutter/material.dart';
import 'menuBar.dart';

void main() {
  runApp(UiREG());
}

class UiREG extends StatefulWidget {
  @override
  State<UiREG> createState() => _UiREGState();
}

class _UiREGState extends State<UiREG> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: appBarMain(),
        body: bodyMain(),
      ),
    );
  }
}

bodyMain() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
              margin: const EdgeInsets.only(top: 1, bottom: 8),
              child: Theme(
                  data: ThemeData(
                    iconTheme: IconThemeData(
                      color: Colors.amber,
                    ),
                  ),
                  child: profileActionItem())),
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 200,
            child: Image.network(
              "https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png",
              fit: BoxFit.contain,
            ),
          ),
          Divider(
            color: Colors.lightBlueAccent,
          ),
          Container(
            height: 68,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "ตารางเรียน",
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.lightBlueAccent,
          ),

          //mobilePhoneListfile(),
          //mobilePhoneListfile2(),
          //mobilePhoneListfileEmail(),
          // mobilePhoneListfileLocation(),
        ],
      ),
    ],
  );
}

Widget profileActionItem() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionButton(),
      //buildPayButton(),
    ],
  );
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.home,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("หน้าหลัก"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.app_registration,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("ลงทะเบียน"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.app_registration_rounded,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("ผลการลงทะเบียน"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.schedule,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("ตางรางเรียน"),
    ],
  );
}

Widget buildDirectionButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.more_horiz,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {
          NavBar;
        },
      ),
      Text("เพิ่มเติม"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.payment,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListfile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-880-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mobilePhoneListfile2() {
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mobilePhoneListfileEmail() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("vergil@vergil.com"),
    subtitle: Text("work"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mobilePhoneListfileLocation() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("234 Sunset St.Burlingame"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

appBarMain() {
  return AppBar(
    backgroundColor: Colors.grey.shade500,
    title: Text("Reg BUU"),
    leading: Icon(
      Icons.arrow_back,
      //color: Colors.white,
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.login_outlined),
        //color: Colors.black,
        onPressed: () {},
      ),
    ],
  );
}

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer();
  }
}
